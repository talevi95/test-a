// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC1rwZwCKeAS2RRYoe1zpTi70cTfyxZoDI",
    authDomain: "test-a-angular.firebaseapp.com",
    projectId: "test-a-angular",
    storageBucket: "test-a-angular.appspot.com",
    messagingSenderId: "130737213003",
    appId: "1:130737213003:web:a4a45ea411ae8969a247ce"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
