import { AuthService } from './../auth.service';
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Student } from '../interfaces/student';
import { PredictionService } from '../prediction.service';
import { StudentsService } from '../students.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  // [x: string]: any;
  // predictClick:boolean = false;

  // city:string;
  // math: number;
  // sat: number;
  // pay: boolean = false;
  // result:string;
  // id: string;
  // selectedOption:boolean;
  // options = [{value:false,viewValue:'will not drop'},{value:true,viewValue:'will drop'}];
  // email:string;


  constructor(private PredictionService:PredictionService,private router:Router) { }
  name:string;


  saveCity(){
    this.PredictionService.saveCity(this.name).subscribe(
      res => this.router.navigate(['/students'])
    )
 }

  ngOnInit(): void {
  }

}
