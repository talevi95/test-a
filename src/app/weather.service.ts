import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Weather } from './interfaces/City';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { City } from './interfaces/City';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "3a99a17608d657538e1afbfff2a7e820";
  private IMP = "units=metric";

  constructor(private http:HttpClient) { }

  // searchWeatherData(cityName:string):Observable<Weather>{
  //   return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
  //     map(data => this.transformWeatherData(data)),
  //     catchError(this.handleError)
  //   )
  // }


  private transformWeatherData(data:WeatherRaw):City{
    return {
      name:data.name,
      image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      temperature:data.main.temp,
      humidity:data.main.humidity,
      wind:data.wind.speed
      }
  }

  searchWeatherData(cityName:string):Observable<City>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }





  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server Error');
  }






  // private transformWeatherData(data:WeatherRaw):City{
  //   return {
  //     name:data.name,
  //     country:data.sys.country,
  //     image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
  //     temperature:data.main.temp,
  //     humidity:data.main.humidity
  //   }
  // }
  


}
















