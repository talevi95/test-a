// import { City } from './../interfaces/City';
// import { WeatherService } from './../weather.service';
// import { Student } from './../interfaces/student';
// import { StudentsService } from './../students.service';
// import { Component, OnInit } from '@angular/core';
// import { AuthService } from '../auth.service';
// import { Observable } from 'rxjs';
// // import { Weather } from '../interfaces/City';

// @Component({
//   selector: 'app-students',
//   templateUrl: './students.component.html',
//   styleUrls: ['./students.component.css']
// })
// export class StudentsComponent implements OnInit {

//   students: Student[];
//   students$;
//   able:boolean= true;
  

//   // city:string;
//    temperature:number; 
//    image:string;
//    humidity:number;
//   // country:string; 
//   // weatherData$:Observable<Weather>;
//   // hasError:boolean = false;
//   // errorMessage:string;

//   cities$:Observable<any>;
//   cities:City[] = []
//   weatherData$: any;
//   hasError: boolean;
//   errorMessage: any;
//   city: string;



//   displayedColumns: string[] = ['city','humidity','temperature','wind','icon','GetData','predict','result' ,'Delete'];
//   route: any;

//   constructor(private StudentsService: StudentsService, public authService: AuthService, public weatherService: WeatherService) { }

//   deleteStudent(sid){
//     console.log(sid);
//     this.StudentsService.deleteStudent(sid);
//   }
//   //able predict buttum
//   GetDataClick(){
//     console.log();
//     this.able=!this.able;
//   }

//   // GetTempData(): void {
//   //   this.city = this.route.snapshot.params.city; 
//   //   this.weatherData$ = this.weatherService.searchWeatherData(this.city);
//   //   this.weatherData$.subscribe(
//   //     data => {
//   //       this.temperature = data.temperature;
//   //       this.image = data.image;
//   //       this.humidity = data.humidity;
//   //       // this.country = data.country;
//   //     },
//   //     error => {
//   //       console.log(error.message);
//   //       this.hasError = true;
//   //       this.errorMessage = error.message;
//   //     }
//   //   )
//   // }

//   getDataFun(index, name){
//     console.log(name);
    
//     this.weatherData$=this.weatherService.searchWeatherData(name).subscribe(
//       data => {
//         console.log(data);
//         this.cities[index].temperature = data.temperature;
//         this.cities[index].image = data.image;
//         this.cities[index].humidity = data.humidity;
//         this.city = name;
     
//       },
//       error=>{
//         console.log(error.message);
//         this.hasError= true;
//         this.errorMessage=error.message;
//       }
//     )
//   }




//   ngOnInit(): void {
//     this.students$ = this.StudentsService.getStudents();
//     this.students$.subscribe(
//       docs => {
//         console.log(docs);
//         this.students = [];
//         var i = 0;
//         for (let document of docs) {
//           console.log(i++);
//           const student: Student = document.payload.doc.data();
//           //student.id = document.payload.doc.id;
//           console.log(document.payload.doc.data());
//           this.students.push(student);
//         }
//       }
//     )
//     console.log(this.students)
//   }
// }



import { getTestBed } from '@angular/core/testing';
import { AuthService } from './../auth.service';
import { PredictionService } from './../prediction.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from '../interfaces/city';

@Component({
    selector: 'app-students',
    templateUrl: './students.component.html',
    styleUrls: ['./students.component.css']
  })
  export class StudentsComponent implements OnInit {
  cities$:Observable<any>;
  cities:City[] = []
  displayedColumns: string[] = ['name','getdata','predict','temperature','humidity','windspeed','image','result','delete'];
  weatherData$: any;
  hasError: boolean;
  errorMessage: any;
  city: string;
  result$:any;
  owner: any;



  constructor(private PredictionService:PredictionService,private AuthService:AuthService) { }

  predict(index){
    // console.log(index);
    // console.log(this.cities[index]);
    this.result$=this.PredictionService.predict(this.cities[index].temperature,this.cities[index].humidity);
    this.result$=this.result$.subscribe(
      result=>{
        console.log(result);
        if (result>50){
          this.cities[index].predict = "rain";
        }
        else {
          this.cities[index].predict = "no rain";
        }


      }
      
    )
  }

  add(index){
    this.PredictionService.addPredict(this.cities[index].name,this.cities[index].temperature,this.cities[index].humidity,this.cities[index].wind,this.cities[index].predict,this.owner); 
  // this.cities[index].saved = true;
}

deletecity(id){
     console.log(id);
     this.PredictionService.deleteCity(id);
     }



  getDataFun(index, name){
    console.log(name);
    
    this.weatherData$=this.PredictionService.searchWeatherData(name).subscribe(
      data => {
        console.log(data);
        this.cities[index].temperature = data.temperature;
        this.cities[index].image = data.image;
        this.cities[index].humidity = data.humidity;
        this.cities[index].wind = data.wind;
        this.city = name;
     
      },
      error=>{
        console.log(error.message);
        this.hasError= true;
        this.errorMessage=error.message;
      }
    )
  }



  ngOnInit(): void {
    this.AuthService.getUser().subscribe(

      user => {
        this.owner=user.email;
        })
    this.cities$ = this.PredictionService.getCities();
    this.cities$.subscribe(
      docs => {
        this.cities =[];
        for(let document of docs){
          const newCity:City = document.payload.doc.data();
          newCity.id = document.payload.doc.id;
          this.cities.push(newCity);
        }
      }
    )
  }

}